mkdir html
mkdir html/js

# Download & install underscore.js
git clone https://github.com/jashkenas/underscore.git ./underscore
cp underscore/underscore-min.js html/js/underscore.js
rm -rf underscore

# Download & install jQuery
curl 'http://code.jquery.com/jquery-2.0.3.min.js' > html/js/jquery.js

# Download & install require.js
git clone https://github.com/jrburke/requirejs.git ./requirejs
cp requirejs/require.js html/js/require.js
rm -rf requirejs


# Generate a blank HTML
(
cat <<EOT
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-type" value="text/html;charset=utf-8" />
        <script type="text/javascript" data-main="js/main" src="js/require.js"></script>
    </head>
    <body>
    </body>
</html>
EOT
) > html/index.html

# Generate a main JS file
(
cat <<'EOT'
// Main entry point.
require(['underscore', 'jquery'], function (){
    $(document).ready(function(){
        alert("all is fine");
    });
});
EOT
) > html/js/main.js
